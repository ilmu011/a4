library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_unsigned.all;
    
entity counter is                                 -- Device Under test 
    port(
    err   : out std_logic;                        -- ERRor : invalid counter value 
    cnt   : out std_logic_vector( 11 downto 0 );  -- CouNT 
                                                  --
    valLd : in  std_logic_vector( 11 downto 0 );  -- init VALue in case of LoaD 
    nLd   : in  std_logic;                        -- Not LoaD; low actve LoaD 
    nClr  : in  std_logic;                        -- Not CLeaR : low actve CLeaR 
    up    : in  std_logic;                        -- UP count command 
    down  : in  std_logic;                        -- DOWN count command 
                                                  --
    clk   : in  std_logic;                        -- CLocK 
    nres  : in  std_logic                         -- Not RESet ; low active synchronous reset 
    );--]port 
end entity counter;

architecture counter_arop of counter is
Signal valLd_ns : std_logic_vector(11 downto 0);
Signal nLd_ns   : std_logic;
Signal nClr_ns  : std_logic;
Signal up_ns    : std_logic;
Signal down_ns  : std_logic; 

Signal valLd_cs : std_logic_vector(11 downto 0) := (others => '0');
Signal nLd_cs   : std_logic := '0';
Signal nClr_cs  : std_logic := '0';
Signal up_cs    : std_logic := '0';
Signal down_cs  : std_logic := '0';

Signal prev_nLd_cs   : std_logic := '0'; --
Signal prev_nClr_cs  : std_logic := '0'; --
Signal prev_up_cs    : std_logic := '0'; -- Wert dieser Signale im vorherigen Takt (benötigt zur Flankenerkennung)
Signal prev_down_cs  : std_logic := '0'; --

Signal prev_nLd_ns   : std_logic; --
Signal prev_nClr_ns  : std_logic; --
Signal prev_up_ns    : std_logic; -- Wert dieser Signale im vorherigen Takt (benötigt zur Flankenerkennung)
Signal prev_down_ns  : std_logic; --

Signal edge_nLd_s   : std_logic; --
Signal edge_nClr_s  : std_logic; --
Signal edge_up_s    : std_logic; -- Hier werden die Edges gespeichert.
Signal edge_down_s  : std_logic; --

Signal count_ns     : std_logic_vector(11 downto 0);
Signal err_ns       : std_logic;

Signal count_cs     : std_logic_vector(11 downto 0) := (others => '0');
Signal err_cs       : std_logic                     := '0';
Signal saved_cs     : std_logic_vector(11 downto 0) := (others => '0');
Signal saved_err_cs : std_logic                     := '0';

begin
    sumup:
    process(valLd_cs,  
            saved_cs,
            saved_err_cs,
            edge_nLd_s, 
            edge_nClr_s,
            edge_up_s, 
            edge_down_s) is
        
        variable valLd_v             : std_logic_vector(11 downto 0);
        variable edge_nLd_v          : std_logic;
        variable edge_nClr_v         : std_logic;
        variable edge_up_v           : std_logic;
        variable edge_down_v         : std_logic;
        variable count_v             : std_logic_vector(11 downto 0);
        variable result_v            : std_logic_vector(12 downto 0);    
        variable err_v               : std_logic;                        
        variable saved_v             : std_logic_vector(11 downto 0);
        variable saved_err_v         : std_logic;
        variable summand1_v          : std_logic_vector(12 downto 0);
        variable summand2_v          : std_logic_vector(12 downto 0);
        variable updownsel_v         : std_logic_vector(1 downto 0);
        constant thirteenbitZero     : std_logic_vector(12 downto 0) := "0000000000000";
        constant thirteenbitOne      : std_logic_vector(12 downto 0) := "0000000000001";
        constant thirteenbitMinusOne : std_logic_vector(12 downto 0) := "1111111111111";
        
    begin
        --Schritt 1
        valLd_v  := valLd_cs;
        saved_v  := saved_cs;
        
        if(edge_nClr_v = '1') then
           err_v := '0';
        else
           err_v := saved_err_cs;
        end if;
        
        edge_nLd_v    := edge_nLd_s;
        edge_nClr_v   := edge_nClr_s;
        edge_up_v     := edge_up_s;
        edge_down_v   := edge_down_s;
        
        --Schritt 2
        
        if(edge_nLd_v = '1') then -- EDGE
            summand1_v(11 downto 0) := valLd_v;
        else
            summand1_v(11 downto 0) := saved_v;
        end if;
        
        summand1_v(12) := '0';
        
        updownsel_v := ((edge_up_v) & (edge_down_v)); -- EDGE x2
        
        case updownsel_v is
            when "10"   => summand2_v := thirteenbitOne;
            when "01"   => summand2_v := thirteenbitMinusOne;
            when others => summand2_v := thirteenbitZero;
        end case;
        
        result_v := std_logic_vector(summand1_v + summand2_v);
        
        count_v := result_v(11 downto 0);
        
        if(result_v(result_v'high) /= '0') then
            err_v := '1';
        end if;
        
        --Schritt 3
        
        count_ns <= count_v;
        err_ns   <= err_v;
    end process sumup;
    
    sequlo:
    process(clk) is 
    begin 
     if clk = '1' and clk'event then
            if nres = '0' then
                valLd_cs         <= (others => '0');
                nLd_cs           <= '0';
                nClr_cs          <= '0';
                up_cs            <= '0';
                down_cs          <= '0';
                prev_nLd_cs      <= '0';
                prev_nClr_cs     <= '0';
                prev_up_cs       <= '0';
                prev_down_cs     <= '0';
                count_cs         <= (others => '0');
                err_cs           <= '0';
                saved_cs         <= (others => '0');
                saved_err_cs     <= '0';
            else
                valLd_cs         <= valLd_ns;
                nLd_cs           <= nLd_ns; 
                nClr_cs          <= nClr_ns;
                up_cs            <= up_ns; 
                down_cs          <= down_ns;
                prev_nLd_cs      <= prev_nLd_ns;
                prev_nClr_cs     <= prev_nClr_ns;
                prev_up_cs       <= prev_up_ns;
                prev_down_cs     <= prev_down_ns;
                count_cs         <= count_ns;
                err_cs           <= err_ns;
                saved_cs         <= count_ns;
                saved_err_cs     <= err_ns;
            end if;
        end if;
    end process sequlo;
    
    valLd_ns <= valLd; 
    nLd_ns   <= nLd;
    nClr_ns  <= nClr;
    up_ns    <= up;
    down_ns  <= down;

    cnt      <= count_cs;
    err      <= err_cs;
    
    prev_nLd_ns   <= nLd_cs;  --
    prev_nClr_ns  <= nClr_cs; --
    prev_up_ns    <= up_cs;   -- Wert dieser e im vorherigen Takt (benötigt zur Flankenerkennung)
    prev_down_ns  <= down_cs; --
    
    edge_nLd_s    <= (NOT nLd_cs) AND prev_nLd_cs;
    edge_nClr_s   <= (NOT nClr_cs) AND prev_nClr_cs;
    edge_up_s     <= up_cs AND (NOT prev_up_cs);
    edge_down_s   <= down_cs AND (NOT prev_down_cs);
end architecture counter_arop;