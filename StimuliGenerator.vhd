library work;
    use work.all;

library std;
    use std.textio.all;                 -- benoetigt vom "markStart"-PROCESS

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;       -- benoetigt fuer conv_std_logic_vector() (und Entscheidung gegen "numeric_std_unsigned")
    use ieee.std_logic_unsigned.all;    -- benoetigt fuer conv_std_logic_vector() (und Entscheidung gegen "numeric_std_unsigned")



-- sg ::= Stimuli Generator    
entity StimuliGenerator is
    port (
        data : out std_logic_vector( 11 downto 0);
        ld   : out std_logic;
        clr  : out std_logic;
        up   : out std_logic;   
        down : out std_logic;
        clk  : out std_logic;
        nres : out std_logic
    );--]port
end entity StimuliGenerator;



architecture beh of StimuliGenerator is
    
    -- Das Arbeiten mit 1/8 Takt-Zyklen ist eigentlich "uebertrieben",
    --   aber es erleichtert das Lesen der Waves
    --   und es bleibt auf den Stimuli Generator beschraenkt
    -- 8*2500ps = 20ns Takt-Periode => 50MHz Takt-Frequenz
    constant oneEigthClockCycle     : time  := 50 ns;
    constant sevenEigthClockCycle   : time  := 7 * oneEigthClockCycle;
    constant halfClockCycle         : time  := 4 * oneEigthClockCycle;
    constant fullClockCycle         : time  := 8 * oneEigthClockCycle;
    
    
    signal   simulationRunning_s    : boolean  := true;     -- for internal signalling that simulation is running 
    
    signal   VDD                    : std_logic  := '1';    -- always one
    signal   GND                    : std_logic  := '0';    -- always zero
    
    signal   nres_s                 : std_logic  := 'L';    -- "internal" nres - workaround to enable reading nres without using "buffer-ports"
    signal   clk_s                  : std_logic;            -- "internal" clk - workaround to enable reading clk without using "buffer-ports"
    
begin
    
    VDD <= '1';
    GND <= '0';
    
    
    
    -- Der folgende PROCESS markiert im Transcript-Fenster das Ende des RESETs
    -- In der Konsequenz muessen dann alle Signale initialisiert und "wohl-definiert" sein
    markStart:                                              -- mark that REST has passed and hence, all signals have to be valid
    process is
        variable wlb : line;
    begin
        -- wait for RESET to appear
        if  nres_s/='0'  then  wait until nres_s='0';  end if;
        -- (low active) RESET is active
        --
        -- wait for RESET to vanish
        wait until nres_s='1';
        -- RESET has passed
        
        -- mark that all signals have to be "valid"
        write( wlb, string'( "###############################################################################" ) );     writeline( output, wlb );
        write( wlb, string'( "###############################################################################" ) );     writeline( output, wlb );
        write( wlb, string'( "###############################################################################" ) );     writeline( output, wlb );
        write( wlb, string'( "###" ) );                                                                                 writeline( output, wlb );
        write( wlb, string'( "###" ) );                                                                                 writeline( output, wlb );
        write( wlb, string'( "###   RESET has passed - all signals have to be valid  @ " ) );
        write( wlb, now );                                                                                              writeline( output, wlb );
        write( wlb, string'( "###" ) );                                                                                 writeline( output, wlb );
        
        wait;
    end process markStart;
    --
    --
    --
    resGen:                                                 -- RESet GENerator
    process is
    begin
        nres_s <= '0';                                      -- set low active reset
        for i in 1 to 2 loop
            wait until '1'=clk_s and clk_s'event;           -- wait for rising clock edge
        end loop;
        -- since 2 rising clk edges have passed, synchronous reset must have been executed
        
        wait for oneEigthClockCycle;
        nres_s <= '1';                                      -- clear low active reset
        -- 1/8 clk period after rising clk edge reset is vanishing
        
        wait;
    end process resGen;
    --
    nres <= nres_s;
    
    
    
    
    
    clkGen:                                                 -- CLocK GENerator
    process is
    begin
        clk_s <= '0';
        wait for fullClockCycle;
        while simulationRunning_s loop
            clk_s <= '1';
            wait for halfClockCycle;
            clk_s <= '0';
            wait for halfClockCycle;
        end loop;
        wait;
    end process clkGen;
    --
    clk <= clk_s;
    
    
    
    
    
    sg:                                                     -- Stimuli Generator
    process is
    begin
        simulationRunning_s <= true;                        -- redundant as result of power-up initialisation - but however, it looks nicer
        
        if  nres_s/='0'  then  wait until nres_s='0';  end if;
        data <= (others=>'0');                              -- just set any defined data
        ld   <= '1';
        clr  <= '1';
        up   <= '0';
        down <= '0';
        if  nres_s/='1'  then  wait until nres_s='1';  end if;
        --reset has passed
        
        
        
        -- give CPLD some time to wake up
        for  i in 1 to 10 loop
            wait until '1'=clk_s and clk_s'event;           -- wait for rising clock edge
        end loop;
        -- CPLD is configured and able to react
        
        
        
        -- "HIER" kommt der "eigentliche Test"
        -- Beispielhaft werden nun ein paar Testmuster erzeugt
        
        -- UP TESTEN <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        wait for oneEigthClockCycle;
        up <= '1';
        
        for i in 0 to 10 loop
            wait until '1'=clk_s and clk_s'event;
        end loop;
        
        wait for oneEigthClockCycle;
        up <= '0';
        
        for i in 0 to 10 loop
            wait until '1'=clk_s and clk_s'event;
        end loop;
        
        wait for oneEigthClockCycle;
        up <= '1';
        
        for i in 0 to 10 loop
            wait until '1'=clk_s and clk_s'event;
        end loop;
        
        wait for oneEigthClockCycle;
        up <= '0';
        
        for i in 0 to 10 loop
            wait until '1'=clk_s and clk_s'event;
        end loop;
        -- DOWN TESTEN <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        
        wait for oneEigthClockCycle;
        down <= '1';
        
        for i in 0 to 10 loop
            wait until '1'=clk_s and clk_s'event;
        end loop;
        
        wait for oneEigthClockCycle;
        down <= '0';
        
        for i in 0 to 10 loop
            wait until '1'=clk_s and clk_s'event;
        end loop;
        
        wait for oneEigthClockCycle;
        down <= '1';
        
        for i in 0 to 10 loop
            wait until '1'=clk_s and clk_s'event;
        end loop;
        
        wait for oneEigthClockCycle;
        down <= '0';
        
        for i in 0 to 10 loop
            wait until '1'=clk_s and clk_s'event;
        end loop;
        
        --ERROR TESTEN<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        
        
        
        wait for oneEigthClockCycle;
        data <= conv_std_logic_vector(4095, 12);
        ld <= '0';
        
        wait until '1'=clk_s and clk_s'event;
        
        wait for oneEigthClockCycle;
        ld <= '1';
        
        for i in 0 to 10 loop
            wait until '1'=clk_s and clk_s'event;
            wait for oneEigthClockCycle;
            up <= '1';
            wait until '1'=clk_s and clk_s'event;
            wait for oneEigthClockCycle;
            up <= '0';
        end loop;
        
        for i in 0 to 10 loop
            wait until '1'=clk_s and clk_s'event;
        end loop;
        
        wait for oneEigthClockCycle;
        clr <= '0';
        
        for i in 0 to 10 loop
            wait until '1'=clk_s and clk_s'event;
        end loop;
        
        clr <= '1';
        
        for i in 0 to 10 loop
            wait until '1'=clk_s and clk_s'event;
            wait for oneEigthClockCycle;
            down <= '1';
            wait until '1'=clk_s and clk_s'event;
            wait for oneEigthClockCycle;
            down <= '0';
        end loop;
        
        --wait for oneEigthClockCycle;
        data <= conv_std_logic_vector(2000, 12);
        ld <= '0';
        
        wait until '1'=clk_s and clk_s'event;
        
        wait for oneEigthClockCycle;
        ld <= '1';
        
        
        
        -- Der "eigentliche Test" ist nun zu Ende
        
        
        
        -- stop SG after 10 clk cycles - assuming computed data is stable afterwards
        for  i in 1 to 10 loop
            wait until '1'=clk_s and clk_s'event;           -- wait for rising clock edge
        end loop;
        simulationRunning_s <= false;                       -- stop clk generation
        --
        wait;
    end process sg;
    
end architecture beh;
