library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    
entity counter_testbench is
end entity counter_testbench;

architecture rtl of counter_testbench is
    Signal valLd_s  :   std_logic_vector(11 downto 0);
    Signal nLd_s    :   std_logic;
    Signal nClr_s   :   std_logic;
    Signal up_s     :   std_logic;
    Signal down_s   :   std_logic;
    
    Signal count_s  :   std_logic_vector(11 downto 0);
    Signal err_s    :   std_logic;
    
    Signal clk_s    : std_logic;
    Signal nres_s   : std_logic;


    Component counter is
        port ( 
        err   : out std_logic;                        -- ERRor : invalid counter value 
        cnt   : out std_logic_vector( 11 downto 0 );  -- CouNT 
        
        valLd : in  std_logic_vector( 11 downto 0 );  -- init VALue in case of LoaD 
        nLd   : in  std_logic;                        -- Not LoaD; low actve LoaD 
        nClr  : in  std_logic;                        -- Not CLeaR : low actve CLeaR 
        up    : in  std_logic;                        -- UP count command 
        down  : in  std_logic;                        -- DOWN count command 
        
        clk   : in  std_logic;                        -- CLocK 
        nres  : in  std_logic                         -- Not RESet ; low active synchronous reset 
      );--]port
    end Component counter;
    
    for all : counter  use entity work.counter(counter_arop); --counter_arop ODER countertimesim
    
    Component StimuliGenerator is
        port (
        data : out std_logic_vector( 11 downto 0);
        ld   : out std_logic;
        clr  : out std_logic;
        up   : out std_logic;   
        down : out std_logic;
        clk  : out std_logic;
        nres : out std_logic
    );--]port
    end Component StimuliGenerator;
    
    for all : StimuliGenerator use entity work.StimuliGenerator(beh);
    
begin
    sg : StimuliGenerator
    port map(
        data => valLd_s,
        ld   => nLd_s,
        clr  => nClr_s,
        up   => up_s,
        down => down_s,
        
        clk  => clk_s,
        nres => nres_s
        );
        
    dut : counter
    port map(
        err => err_s,
        cnt => count_s,
        valLd => valLd_s,
        nLd => nLd_s,
        nClr => nClr_s,
        up => up_s,
        down => down_s,
        
        clk => clk_s,
        nres => nres_s
        );
        

end architecture rtl;