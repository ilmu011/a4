onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider -height 30 {CLOCK & RESET}
add wave -noupdate -label Clock /counter_testbench/clk_s
add wave -noupdate -label Reset /counter_testbench/nres_s
add wave -noupdate -divider -height 30 INPUT
add wave -noupdate -label valLd -radix unsigned /counter_testbench/valLd_s
add wave -noupdate -label nLd /counter_testbench/nLd_s
add wave -noupdate -label nClr /counter_testbench/nClr_s
add wave -noupdate -label up /counter_testbench/up_s
add wave -noupdate -label down /counter_testbench/down_s
add wave -noupdate -divider -height 30 OUTPUT
add wave -noupdate -label count -radix unsigned /counter_testbench/count_s
add wave -noupdate -label err /counter_testbench/err_s
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ns} 0} {{Cursor 2} {1000 ns} 0}
quietly wave cursor active 2
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {2046 ns} {3046 ns}
